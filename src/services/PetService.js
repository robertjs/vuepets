import Api from '@/services/Api'

export default {
  post (pet) {
    return Api().post('pets', pet)
  },
  put (pet) {
    return Api().put('pets', pet)
  },
  delete (pet) {
    return Api().delete('pets', pet)
  }

}
