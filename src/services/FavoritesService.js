
import Api from '@/services/Api'

export default {
  index (favorite) {
    return Api().get('favorites', {
      params: favorite
    })
  },
  post (payload) {
    return Api().post('favorites', payload)
  },
  delete (favoriteId) {
    return Api().delete('favorites', { params: { favoritedId: favoriteId } })
  }
}
