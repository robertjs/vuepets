import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Pets from '@/components/Pets'
import store from '@/store/store'
import UserProfile from '@/components/UserProfile'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    beforeEnter: (to, from, next) => {
      if (!store.getters.user.isAdmin) {
        window.confirm('You do not have access')
        next('/login')
      } else {
        next()
      }
    },
    component: () => import('../views/Admin.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/pets',
    name: 'pets',
    component: Pets
  },
  {
    path: '/profile',
    name: 'profile',
    component: UserProfile
  }
]

const router = new VueRouter({
  routes
})

export default router
